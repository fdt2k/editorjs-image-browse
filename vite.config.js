const { join, resolve } = require('path');

import fs from 'fs/promises';
import pkg from './package.json';

const PACKAGE_ROOT = __dirname;

console.log('ALIAS', resolve(PACKAGE_ROOT, 'src'))

module.exports = {
  base: '',
  define: {
    "process.env.RENDERER_VERSION": `"${pkg.version}"`,
  },
  target: 'web',

  esbuild: {
    loader: "jsx",
    include: /src\/.*\.jsx?$/,
    // loader: "tsx",
    // include: /src\/.*\.[tj]sx?$/,
    exclude: [],
  },
 
  root: PACKAGE_ROOT,
  resolve: {
    alias: {
      '@': resolve(PACKAGE_ROOT, './src') + '/',
    },
  },
  build: {
    minify: false,

    lib: {
      entry: resolve(PACKAGE_ROOT, 'src/index.js'),
      name: pkg.kda.component.name,
      formats: pkg.kda.build.formats,
     
      fileName: (format) => `${pkg.kda.build.filename}.${format}.js`
    },
    rollupOptions: {
      external: require('./externals').default,
      output:{
        globals:{
          react: 'React',
          "react-dom": 'ReactDOM',
          "prop-types": 'PropTypes'
        },
      }
    },
  }
};