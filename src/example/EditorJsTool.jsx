import Paragraph from '@editorjs/paragraph'
import SimpleImage from '@';
export const EDITOR_JS_TOOLS = {
    paragraph: {
        class: Paragraph,
        config: {
            preserveBlank: true
        },
        inlineToolbar: true

    },

    image: {
        class:SimpleImage,
        inlineToolbar: ['link'],
       
        config:{
            previewClass:'hello-preview',
            shouldMakeLinkAbsolute:true,
            browseCallback : function (callback){

                console.log('browse clicked');

                callback('/logo512.png');
            }
        }
    }
};
